package ru.t1.vlvov.tm.api.service.model;

import ru.t1.vlvov.tm.api.repository.model.IUserOwnedRepository;
import ru.t1.vlvov.tm.model.AbstractUserOwnedModel;

public interface IUserOwnedService<M extends AbstractUserOwnedModel> extends IService<M>, IUserOwnedRepository<M> {
}
